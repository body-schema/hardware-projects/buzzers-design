# buzzers-design

## Overview
This board utilizes 16 channel PWM DAC for buzzer driving. 
Device generates vibration and impulse stimulis.
The core of the device is RPI Pico.
The device communicates with commanding PC over Bluetooth interface.
As a supply 18650 batteries are used.

## Hardware
Device exploits all 16 PWM channels for generating sine signals. 
To generate sinusoidal signal 1MHz PWM is used.
Transistor CMOS invertor stage is used to boost current outputing capabilities.
Output is the coupled using high capacity electrolyte capacitors.
This capacitor forms a Highpass filter thanks to which we can generate strong impulses.

## Software
Device runs a code writen in Arduino IDE.
Arduino was chosen for easy Bluetooth connection to PC.
Rest of the software is writen using PICO SDK (mainly to ensure correct and more reliable function).

### Warning
Even though the device can use external power source to boost speakers performance, voltage higher than about 6-7V is not recomended as transitors tend to overheat. This is most likely due to the power losses while toggling output states, to fix this issue mosfet drivers or FETs with lower gate charge should be used. However, we find performance of buzzers sufficient even without external power source.

Another concern is weather sinusiodal driving is actually needed as lower frequency square wave should result in lower power losses.
 
![Front side](/figs/front.png)
![Back side](/figs/back.png)
