/*
  in library file Arduino/libraries/CmdMessenger/CmdMessenger.cpp, line 492 changed: return '\0' -> return NULL
*/

#include "CmdMessenger.h"

#include "RP2040_PWM.h"
#include "math.h"


#define NUM_OF_PORTS 16

#define PWM_BASE_PTR (uint32_t *)0x40050000
#define PWM_CHANNEL_OFFSET 0x14
#define PWM_CSR_OFFSET 0x00
#define PWM_DIV_OFFSET 0x04
#define PWM_CTR_OFFSET 0x08
#define PWM_CC_OFFSET 0x0c
#define PWM_TOP_OFFSET 0x10


#define portA1 2
#define portA2 3
#define portA3 4
#define portA4 5

#define portB1 6
#define portB2 7
#define portB3 8
#define portB4 9

#define portC1 10
#define portC2 11
#define portC3 12
#define portC4 13

#define portD1 14
#define portD2 15
#define portD3 16
#define portD4 17

uint8_t ports[] = { portA1, portA2, portA3, portA4, portB1, portB2, portB3, portB4, portC1, portC2, portC3, portC4, portD1, portD2, portD3, portD4 };
uint sliceNums[16];

RP2040_PWM *PWM_Instances[NUM_OF_PORTS];

float t = 0;
float fs = 7500;
float fpwm = 1000000;
float impulse_period = 0.01;
uint32_t wait_us = 1e6/fs;
uint32_t fminimal = 1;

bool run = false;


struct {
  // buzzing, for legacy reasons struct still contains sub-struct buzzing, which is used for both buzzing and impulses.
  struct {
    uint16_t *wavetables[NUM_OF_PORTS];     // table for signal to be pushed to the PWM
    uint32_t sizesOfTable[NUM_OF_PORTS];    // lenght of valid data in the table (pariod of the sine wave or duration of impulse)
    uint32_t iterators[NUM_OF_PORTS];       // iterator for given table
    uint32_t total_iterators[NUM_OF_PORTS]; // iterator for the whole signal to be generated. 
    uint32_t max_iterators[NUM_OF_PORTS];   // max iterator for the whole signal to be generated.
                                            // sizesOfTable[i] * number_of_periodes == max_iterators[i]
  } buzzing;

} setting;


CmdMessenger cmdMessenger = CmdMessenger(Serial1);
enum {
  // Commands
  kBuzz,     // Command to set buzzing
  kImpulse,  // Command to set impulses
  kToggle,   // Command to toggle buzzing or pulsing - unused, might be used in a future
  kResponse
};

void attachCommandCallbacks();
void OnToggle();
void OnBuzz();
void OnImpulse();

void initSetting();
mutex_t *settingsLock;

uint32_t *base;
uint32_t *adr;
uint32_t tmpReg;

uint64_t startOfTheLoop;
uint64_t endOfTheLoop;

void setup1() {
  initSetting();
  Serial1.begin(115200);
  attachCommandCallbacks();
}

void setup() {
  // set clocks
  clocks_init();
  clock_configure(clk_sys,
                    CLOCKS_CLK_REF_CTRL_SRC_VALUE_XOSC_CLKSRC,
                    CLOCKS_CLK_SYS_CTRL_AUXSRC_VALUE_CLKSRC_PLL_SYS ,
                    12* MHZ,
                    125 * MHZ);
  // init and set all ports
  for (uint8_t i = 0; i < 16; i++) {
    gpio_set_function(ports[i], GPIO_FUNC_PWM);
    sliceNums[i] = pwm_gpio_to_slice_num(ports[i]);
    pwm_set_clkdiv_int_frac(sliceNums[i], 1, 4);
    pwm_set_wrap(sliceNums[i], 1000);             // timer wraps on 1000 (-1 on sine wave corresponds to 0, 1 to 1000)
    pwm_set_enabled(sliceNums[i], true);
    pwm_set_gpio_level(ports[i], 0);
  }

  mutex_init(settingsLock);
  pinMode(LED_BUILTIN, OUTPUT);


  delay(1000);      // wait for a second for the other setup to finish, some bool "semaphore" would be better
}

void loop1() {
  cmdMessenger.feedinSerialData();
}

void loop() {

  startOfTheLoop = time_us_64();
  // buzzing
  for (uint8_t i = 0; i < NUM_OF_PORTS; i++) {
    if (setting.buzzing.total_iterators[i] < setting.buzzing.max_iterators[i]) {
      pwm_set_gpio_level(ports[i], setting.buzzing.wavetables[i][(setting.buzzing.iterators[i])]);
      if (++setting.buzzing.iterators[i] >= setting.buzzing.sizesOfTable[i]) setting.buzzing.iterators[i] = 0;
      setting.buzzing.total_iterators[i]++;

      
    } else {
      pwm_set_gpio_level(ports[i], 0);
      if (++setting.buzzing.iterators[i] >= setting.buzzing.sizesOfTable[i]) setting.buzzing.iterators[i] = 0;
      setting.buzzing.total_iterators[i]++;
      
    }
  }
  endOfTheLoop = time_us_64();
  delayMicroseconds(wait_us - (endOfTheLoop-startOfTheLoop));
}

void initSetting() {
  for (uint8_t index = 0; index < NUM_OF_PORTS; index++) {
    setting.buzzing.wavetables[index] = (uint16_t *)malloc(sizeof(uint16_t) * (uint32_t)(fs / fminimal));

    for(uint32_t k=0; k < (uint32_t)(fs / fminimal); k++) {
      setting.buzzing.wavetables[index][k] = (uint16_t) 0;
    }

    setting.buzzing.sizesOfTable[index] = 0;
    setting.buzzing.iterators[index] = 0;
    setting.buzzing.total_iterators[index] = 0;
    setting.buzzing.max_iterators[index] = 0;
  }
}

void attachCommandCallbacks() {
  // Attach callback methods
  cmdMessenger.attach(kToggle, OnToggle);
  cmdMessenger.attach(kBuzz, OnBuzz);
  cmdMessenger.attach(kImpulse, OnImpulse);
}

void OnToggle() {
  // toggle set output
  run = !run;
  cmdMessenger.sendCmdStart(kResponse);
  cmdMessenger.sendCmdBinArg<byte>(1);
  cmdMessenger.sendCmdEnd();
}

void OnBuzz() {

  char *p = cmdMessenger.readStringArg();         // port
  float amp = cmdMessenger.readBinArg<float>();   // amplitude
  float freq = cmdMessenger.readBinArg<float>();  // frequency
  float dur = cmdMessenger.readBinArg<float>();   // duration
  uint8_t index = 4 * (p[0] - 'A') + p[1] - '1';
 
  uint32_t sizeOfTable = (uint32_t)(fs / freq);
  
  for (uint32_t k = 0; k < sizeOfTable; k++) { // generate wave table
    setting.buzzing.wavetables[index][k] = (uint16_t)(amp * 500 * sin(2 * M_PI * freq * (k * 1 / fs)) + 500);
  }
  setting.buzzing.sizesOfTable[index] = sizeOfTable;
  setting.buzzing.iterators[index] = 0;
  setting.buzzing.total_iterators[index] = 0;
  setting.buzzing.max_iterators[index] = (uint32_t)(fs * dur);

  cmdMessenger.sendCmdStart(kResponse);
  cmdMessenger.sendCmdBinArg<byte>(1);
  cmdMessenger.sendCmdEnd();
}

void OnImpulse() {
  char *p = cmdMessenger.readStringArg();         // port
  float number = cmdMessenger.readBinArg<int>();  // number of pulses
  float dt = cmdMessenger.readBinArg<float>();    // time between pulses


  uint8_t index = 4 * (p[0] - 'A') + p[1] - '1';
  for(uint32_t k=0; k<(uint32_t)(impulse_period*fs); k++){
    setting.buzzing.wavetables[index][k] = 1000;
  }
  uint32_t offset = (uint32_t)(impulse_period*fs);
  for(uint32_t k=0; k<(uint32_t)((dt-impulse_period)*fs); k++){
    setting.buzzing.wavetables[index][offset + k] = 0;
  }

  setting.buzzing.sizesOfTable[index] = offset + dt*fs;
  setting.buzzing.iterators[index] = 0;
  setting.buzzing.total_iterators[index] = 0;
  setting.buzzing.max_iterators[index] = (uint32_t)((offset + dt*fs)*number);

  cmdMessenger.sendCmdStart(kResponse);
  cmdMessenger.sendCmdBinArg<byte>(1);
  cmdMessenger.sendCmdEnd();
}
	
